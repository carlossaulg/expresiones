
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Evaluador;

import ufps.util.colecciones_seed.*;
import java.util.Stack;
import java.util.*;

/**
 *
 * @author estudiante
 */
public class Expresion {

    private ListaCD<String> expresiones = new ListaCD();

    public Expresion() {
    }

    public Expresion(String cadena) {

        String v[] = cadena.split(",");
        for (String dato : v) {
            this.expresiones.insertarAlFinal(dato);
        }
    }

    public ListaCD<String> getExpresiones() {
        return expresiones;
    }

    public void setExpresiones(ListaCD<String> expresiones) {
        this.expresiones = expresiones;
    }

    @Override
    public String toString() {
        String msg = "";
        for (String dato : this.expresiones) {
            msg += dato + "<->";
        }
        return msg;
    }

    public String getPrefijo() {        
        return "";
    }
    

    public String getPosfijo() {
        Stack<Integer> entrada = new Stack<Integer>();
        Stack<Integer> operadores = new Stack<Integer>();
        Stack<Integer> salida = new Stack<Integer>();
        
        String msg = "";
        for(String dato:this.expresiones){
            entrada.push(Integer.SIZE);
        }
        return msg;
    }

    public float getEvaluarPosfijo() {
        return 0.0f;
    }
    
    private static int pref(String op) {
    int prf = 99;
    if (op.equals("^")) prf = 5;
    if (op.equals("*") || op.equals("/")) prf = 4;
    if (op.equals("+") || op.equals("-")) prf = 3;
    if (op.equals(")")) prf = 2;
    if (op.equals("(")) prf = 1;
    return prf;
  }
}
